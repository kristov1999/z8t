CC=gcc
CFLAGS=-Wall -Werror -ggdb

Z80EX=../z80ex
INCLUDE=-I. -I$(Z80EX)/include

all: z8t

z8t: z8t.c $(Z80EX)/z80ex.o
	$(CC) $(CFLAGS) $(INCLUDE) -o $@ $< $(Z80EX)/z80ex.o

clean:
	rm -f z8t
